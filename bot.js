const Discord = require("discord.js");
const client = new Discord.Client();
var auth = require("./auth.json");
var settings = require("./settings.json");

client.on("ready", () => {
	console.log("Testudo ready.");
	client.user.setPresence({status: 'online' })
	.then(console.log)
	.catch(console.error);
});

client.on("message", msg => {
	console.log(msg.author.username + ": " + msg.content);
	var guild = msg.guild;
	if (msg.author.bot) return;
	if (msg.content.startsWith(settings.prefix)) {
		var cmd = msg.content.substring(settings.prefix.length);
		var args = cmd.split(" ");
		cmd = args[0];
		args.splice(0, 1);

		switch (cmd) {
			case "setmajor":
				if (guild == null) {
					msg.channel.send("You have to run that command in the UMD server. I can't do anything with it here.");
				} else {
					var author = msg.author;
					console.log("author: " + author);
					var member = guild.member(author);
					console.log("member: " + member);
					var userRoles = member.roles;
					// var userRoles = guild.member(msg.author).roles;
					if (args.length <= 4) {
						for (var i = 0; i < settings.majors.length; i ++) {
							var tempRole = guild.roles.find("name", settings.majors[i]);
							if (tempRole != null) {
								userRoles.delete(tempRole.id);
							}
						}
					}
					if (args.length > 0 && args.length <= 4) {
						if (!settings.majors.includes(args[0].toUpperCase())) {
							msg.channel.send("I couldn't find that major. Make sure you have your spelling right, or ask a mod for help.");
						} else {
							var majString = "";
							for (var i = 0; i < args.length; i ++) {
								majString += args[i].toUpperCase() + (i + 2 == args.length? args.length == 2? " and " : ", and " : ", ")
								var role = guild.roles.find("name", args[i].toUpperCase());
								userRoles.set(role.id, role);
							}
								msg.channel.send("I set your major to " + majString  + msg.author.username + ".");
							
						}
					}
					if (args.length == 0) {
						msg.channel.send("I removed your majors, " + msg.author.username + ".");
					}
					guild.member(msg.author).setRoles(userRoles);
				}
				break;
			case "setyear":
				if (guild == null) {
					msg.channel.send("You have to run that command in the UMD server. I can't do anything with it here.");
				} else {
					for (var i = 0; i < settings.years.length; i ++) {
						var tempRole = guild.roles.find("name", settings.years[i]);
						guild.member(msg.author).removeRole(tempRole).catch(console.error);
				}
					if (args.length > 0) {
						if (!settings.years.includes(args[0])) {
							msg.channel.send("I don't have that year in my database. Ask a mod to add it.");
						} else {
							var role = guild.roles.find("name", args[0]);
							guild.member(msg.author).addRole(role);
							msg.channel.send("I set your graduation year to " + args[0] + ", " + msg.author.username + ".");
				 		}
					}
				}
				break;
			case "listmajors":
				msg.channel.send("Majors are designated by their 4 letter course code (e.g. \"CMSC\" = Computer Science).\nA complete list can be found at https://app.testudo.umd.edu/soc/.");
				break;
			case "help":
				msg.channel.send("Testudo help menu.\n" +
						 "setmajor [4 letter major code(s delimited with spaces)]: Sets your major. Use without arguments to clear your major. Must be run in the UMD server.\n" +
						 "listmajors: Lists all available majors. If you don't see yours, ask a moderator to add it.\n" +
						 "setyear [year]: Sets your graduation year. Use without arguments to clear your graduation year. Must be run in the UMD server.\n" +
						 "help: Displays this menu.\n" +
						 "about: Displays Testudo information.");
				break;
			case "about":
				msg.channel.send("Testudo: A bot for the UMD discord server.");
				break;


		}
	} else if (msg.content.startsWith(settings.prefixroot)) {
		var cmd = msg.content.substring(settings.prefixroot.length);
		var args = cmd.split(" ");
		cmd = args[0];
		args.splice(0, 1);

		switch (cmd) {
			case "say":
				if (msg.author.id === "68129361588391936") {
					msg.channel.send(msg.content.replace("Ts say", ""));
				}
				break;
			case "createrole":
				if (guild.member(msg.author).hasPermission(Discord.Permissions.MANAGE_ROLES)) {
					guild.createRole({name: args[0]}).then(role => console.log("Created new role with name " + args[0])).catch(console.error);
				} else {
					msg.channel.send("You cannot do that.");
				}
				break;
			case "removerole":
				if (guild.member(msg.author).hasPermission(Discord.Permissions.MANAGE_ROLES)) {
					if (args.length == 1) {
					guild.roles.find("name", args[0]).delete().then(deleted => console.log("Deleted role with name " + args[0])).catch(console.error);
					} else {
						for (var i = 0; i < args[1]; i ++) {
							console.log("Attempting to delete major of name " + args[1]);
							try {
								guild.roles.find("name", "new role").delete();
							} catch (error) {
								console.log(error);
							}
						}
					}
				} else {
					msg.channel.send("You cannot do that.");
				}
				break;
			case "listusers":
				var userList = "";
			
				break;
			case "populate":
				if (guild.member(msg.author).hasPermission(Discord.Permissions.MANAGE_ROLES)) {
					for (var i = 0; i < settings.majors.length; i ++) {
						console.log("Creating new major of name " + settings.majors[i]);
						guild.createRole({name: settings.majors[i]}).catch(console.error);
					}
					for (var i = 0; i < settings.years.length; i ++) {
						console.log("Creating new year of name " + settings.majors[i]);
						guild.createRole({name: settings.years[i]}).catch(console.error);
					}
				} else {
					msg.channel.send("You cannot do that.");
				}
				break;
			case "depopulate":
				if (guild.member(msg.author).hasPermission(Discord.Permissions.MANAGE_ROLES)) {
					for (var i = 0; i < settings.majors.length; i ++) {
						console.log("Attempting to delete major of name " + settings.majors[i]);
						try {
							guild.roles.find("name", settings.majors[i]).delete();
						} catch (error) {
							console.log(error);
						}
					}
					for (var i = 0; i < settings.years.length; i ++) {
						try {
							guild.roles.find("name", settings.years[i]).delete();
						} catch (error) {
							console.log(error);
						}
					}
				} else {
					msg.channel.send("You cannot do that.");
				}
				break;
			case "removeboostrole":
				if (guild.member(msg.author).hasPermission(Discord.Permissions.MANAGE_ROLES)) {
					try {
						guild.roles.find("name", "Nitro Booster").delete().then(deleted => console.log("Deleted role with name " + args[0])).catch(console.error);
						guild.roles.find("name", "Server Booster").delete().then(deleted => console.log("Deleted role with name " + args[0])).catch(console.error);
					} catch (error) {
						console.log(error);
					}
				} else {
					msg.channel.send("You cannot do that.");
				}
				break;
			case "status":
				msg.channel.send("Active.");
				console.log("status");
				break;
		}
	}
});

client.login(auth.token);
